import React ,{Component} from 'react'
import Form from './Form'

class Expenselist extends Component {

    state ={
        list : [], //value store
    }
    totalExpense = (target) => {
        return this.state.list
          .filter((item) => { // filter , type aur target value ko equal match krta h
            return item.type === target;
          })
          .reduce((acc, cur) => { // to do sum of the values // accumulator(starting point) //cuurent(array value)
            return (acc += Number(cur.value));
          }, 0); 
      };
    
    handleSubmit = (data) => { 
        var list = this.state.list
        list.push(data) 
        this.setState({list}) 
    }
    handleDelete = (e) => {  //e argument
        var list = this.state.list
        list.splice(e,1)
        this.setState({list})
    }

    /*state = {
        list : this.returnList()
    }
    
    returnList (){
        if(localStorage.getItem('value') == null)
        localStorage.setItem('value' , JSON.stringify([]))
        return JSON.parse (localStorage.getItem('value'))
    }
    onAddorEdit = (data) =>{
        var list = this.returnList()
        list.push(data)
        localStorage.setItem('value' , JSON.stringify(list))
        this.setState({list})
    } */


    render(){
        console.log(this.state.list);
        return (
            <><br/><br/>
            <div style={{ border:'solid ' , width:'100%'}}><hr/>
                <Form 
                handleSubmit = {this.handleSubmit} />
                <hr style={{border:'solid '}}/>
                
                <div> 
                <p style={{ border:'solid' , marginLeft:'20px', marginRight:'20px', paddingRight :' 20px', fontWeight:'bolder' , paddingLeft: '10px' , paddingTop:'5px' }}>
                    List of Expenses
                    <table>
                <tbody>
                {this.state.list.map((item, index) => {
              return (
                <tr key={index} className="tr">
                  <td className="td">  {item.desc}</td>
                  <td className="tdh"> $ {item.value}</td>
                  
                  <td className="td"> {item.type}</td>
                  
                  <button
                      className="btn"
                      onClick={() => this.handleDelete(index)}
                      dataset={index}
                    >
                      Delete
                    </button>
                </tr>
              );
            })}
                  </tbody>
                </table> 
                </p>
                <div style={{border:'solid ',marginLeft:'20px', marginRight:'20px', marginbottom:'500px'}}>
          <div style={{border:'solid ',marginLeft:'20px', marginRight:'20px', marginTop:'20px'}} >
            <h3>Travelling:</h3>
            <p>{this.totalExpense("Travelling")}</p>
          </div>
          <div style={{border:'solid ',marginLeft:'20px', marginRight:'20px', marginTop:'20px'}}>
            <h3>Food:</h3>
            <p>{this.totalExpense("Food")}</p>
          </div>
          <div style={{border:'solid ',marginLeft:'20px', marginRight:'20px', marginTop:'20px'}}>
            <h3>HouseRent:</h3>
            <p>{this.totalExpense("HouseRent")}</p>
          </div>
          <hr/>
        </div>
              <hr/>
                </div>
            </div>
            </>
        )
    }
}
export default Expenselist