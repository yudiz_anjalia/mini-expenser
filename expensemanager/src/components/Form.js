
import React,{Component} from 'react'

class Form extends Component{
  constructor(props){
    super(props)
    this.state = {
      value :'',
      type: 'Food',
      desc :'',
  
    }
  }
  

  handleInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.handleSubmit(this.state);
  }


  render(){
      return( 
          
          <div style={{margin :'15px' ,paddingLeft: '5px' , color:'black', border:'solid ', fontWeight:'bolder'}}> <br/>
            Form of Expenses
            <form onSubmit={this.handleSubmit} style={{margin: '20px' , paddingLeft:'5px' }} autoComplete='off'>
          
        <input style={{border:'solid ' , fontWeight:'bold' , width : '120px'}} name = "value" placeholder='Expense' values={this.state.value} onChange={this.handleInput}/> <br/> <br/>
        <input style={{border:'solid ' , fontWeight:'bold' , width : '120px'}} name = "desc" placeholder='Description' values={this.state.desc} onChange={this.handleInput}/> <br/> <br/>
          <label style={{margin :'10px' }} htmlFor='options'>
          Category List
          <div>
            <select style={{margin:'15px' , border:'solid ', color:'balck'}} name='type' values={this.state.type} onChange={this.handleInput}>
              <option value="Travelling">
                Travelling
              </option>
              <option value="Food">
                Food
              </option>
              <option value="HouseRent">
                HouseRent
              </option>
            </select> <br/> <br/>
            <button style={{margin :'15px' , border:'solid '}} onClick={this.handleOnclick}> Add to the List </button>
          </div>
          </label>
        </form>
        
          </div>
      );
  }
}
export default Form 