import React from "react";
import './App.css';
import Expenselist from "./components/Expenselist";

function App (){
  return(
    <Expenselist/>
  );
}
export default App;